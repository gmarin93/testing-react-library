import {readFileSync} from 'fs';  //fs es filesystem para leer archivos del servidor
import path from 'path'; //Para obtener la ubicacion del archivo

//    readFileSync(path.join(__dirname,'api.json')).toString()
//Nos permite leer archivos y con path(__dirname,'NOMBRE ARCHIVO') sin importar donde se encuentre este, lo va a obtener

export const criptos = JSON.parse(
    readFileSync(path.join(__dirname,'api.json')).toString()
);

 export const monedas = [
    { codigo: 'USD', nombre: 'Dolar de Estados Unidos' },
    { codigo: 'MXN', nombre: 'Peso Mexicano' },
    { codigo: 'EUR', nombre: 'Euro' },
    { codigo: 'GBP', nombre: 'Libra Esterlina' }
];