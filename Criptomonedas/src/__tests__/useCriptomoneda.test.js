import React from 'react';
import {render,screen} from '@testing-library/react';
import Formulario from '../components/Formulario';
import userEvent from '@testing-library/user-event';
import {monedas,criptos} from '../__mocks__/criptomonedas';
import axios from 'axios';


const guardarMoneda=jest.fn(); //Estas funciones espias, se comportan como las reales
const guardarCriptomoneda=jest.fn(); 

//Los mockservers permiten realizar consultas de datos falsas para alimentar los datos para realizar las pruebas y evitar el consumo de bandwich en las pruebas, eso puede ahorrar dinero

const mockAxios = axios; //Se puede consumir un archivo json con Axios, siendo el archivo local

test('<useCriptomonedas/>',async()=>{

    //Se inicia la funcion espia, mockResolvedValue finge que se obtienen los datos con el mock server
    mockAxios.get = jest.fn().mockResolvedValue({
        data:criptos
    });

    render(
        <Formulario
        guardarMoneda={guardarMoneda}
        guardarCriptomoneda={guardarCriptomoneda}
        />
    );
        //Verificar las opciones de las monedas
        //monedasDropDown.chldren, trae las opciones del select, recordemos que children son los hijos de la etiqueta padre
    const monedasDropDown = screen.getByTestId('select-monedas'); //Los mocks crean datos como los seeders, datos falsos. De otra manera consumirian mucho bandwich las pruebas
    expect(monedasDropDown.children.length).toEqual(monedas.length+1); //Se valida que el select tenga 5 opciones, con el mock se obtiene la info del api para no realizar muchas consultas por el testing y luego se valida la info en un archivo estatico

    //Verificar la cantidad de opciones de las criptomonedas
    const opciones = screen.findAllByTestId('opcion-cripto') // Se obtienen los datos del api una vez cargados en el componente
    expect( await opciones).toHaveLength(10); //Se esperan 10 registros en el componente de ese api y espera a que se carguen1

    expect(mockAxios.get).toHaveBeenCalled(); //Para validar que se llame la consulta 
    expect(mockAxios.get).toHaveBeenCalledTimes(1); //Para validar que se llame solo una vez, depende del codigo que se tenga puede que sean mas llamados, pero para este ejemplo, es solo una vez

    //Seleccionar Bitcoin y USD, se simula la seleccion en un select
    userEvent.selectOptions(screen.getByTestId('select-monedas'),'USD');
    userEvent.selectOptions(screen.getByTestId('select-criptos'),'BTC');


    //Submit al formulario
    userEvent.click(screen.getByTestId('submit'));

    //Verificar que se llamen las funciones
    expect(guardarMoneda).toHaveBeenCalled(); 
    expect(guardarMoneda).toHaveBeenCalledTimes(1); //Es importante que se verifique que una funcion se llame una vez, para comprobar si hay un loop en la misma o no
    
    expect(guardarCriptomoneda).toHaveBeenCalled(); 
    expect(guardarCriptomoneda).toHaveBeenCalledTimes(1); //Es importante que se verifique que una funcion se llame una vez, para comprobar si hay un loop en la misma o no

});

//render es para montar el componente
//screen es para utilizar y probar las etiquetas como un query.selector

