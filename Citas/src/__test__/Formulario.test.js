import Formulario from '../components/Formulario';
import React from 'react';
import {render,screen,cleanup,fireEvent} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import userEvent from '@testing-library/user-event';

const CrearCita = jest.fn(); //Crea una funcion espia para validar la funcion de agregar citas, se suelen llamar spyFunctions

test('<Formulario /> Cargar el formulario y revisar que todo sea correcto',()=>{
    // const wrapper = render(<Formulario />); 
    // wrapper.debug();  //Imprime todo el componente en pruebas

    // const {getByText} = render(<Formulario />);
    

//En las viejas versiones se utilizaba mucho cleanup, las actuales lo traen importado en cada prueba
    // afterEach(cleanup); //Realiza una limpieza de memoria por cada prueba realizada, cuando se testee otro componente no habra problemas

    render(<Formulario 
        crearCita={CrearCita}
    />);
    expect(screen.getByText('Crear Cita')).toBeInTheDocument(); //Si la alerta existe en el documento

    const btnSubmit = screen.getByTestId('btn-submit');
    fireEvent.click(btnSubmit); //Se valida el click del boton submit enviandole un evento click

    //Heading
    const titulo = screen.getByTestId('titulo');
    expect(titulo.tagName).toBe('H2');
    expect(titulo.tagName).not.toBe('H1');
    expect(titulo.textContent).toBe('Crear Cita');

    //Boton click submit
    expect(screen.getByTestId('btn-submit').tagName).toBe('BUTTON');

    //Revisar alerta
        const alerta = screen.getByTestId('alerta');
        expect(alerta).toBeInTheDocument();
        expect(alerta.textContent).toBe('Todos los campos son obligatorios');
        expect(alerta.tagName).toBe('P');
        expect(alerta.tagName).not.toBe('BUTTON');
}); 

    test('<Formulario /> Validacion del formulario',()=>{

        //Se monta  el formulario
        render(<Formulario 
            crearCita={CrearCita}
        />);

        //Se digita en el formulario
        userEvent.type(screen.getByTestId('mascota'),'Hook'); //Sintaxis mas facil para tipear en los campos, mejor que fireEvent
        userEvent.type(screen.getByTestId('propierario'),'Glenn'); //Sintaxis mas facil para tipear en los campos, mejor que fireEvent
        userEvent.type(screen.getByTestId('fecha'),'2021-09-10'); //Sintaxis mas facil para tipear en los campos, mejor que fireEvent
        userEvent.type(screen.getByTestId('hora'),'10:30'); //Sintaxis mas facil para tipear en los campos, mejor que fireEvent
        userEvent.type(screen.getByTestId('sintomas'),'Solo duerme'); //Sintaxis mas facil para tipear en los campos, mejor que fireEvent

        // fireEvent.change(screen.getByTestId('mascota'),{
        //     target:{
        //         value:'Hook'
        //     }
        // });

        // fireEvent.change(screen.getByTestId('propietario'),{
        //     target:{
        //         value:'Glenn'
        //     }
        // });
        // const btnSubmit = screen.getByTestId('btn-submit');
        // fireEvent.click(btnSubmit); //Se valida el click del boton submit enviandole un evento click

        //Eventos click del formulario
        const btnSubmit = screen.getByTestId('btn-submit');
        userEvent.click(btnSubmit); //Se valida el click

        //Que no se muestre la alerta
        const alerta = screen.queryByTestId('alerta'); //Con qieryByTestId se valida el codigo condicional, porque con getbyTestId estamos preguntando por una etiqueta que afuerza existe.
        expect(alerta).not.toBeInTheDocument(); //No se debe mostrar la alerta

        //Crear cita  y comprobar que se haya llamado la funcion expect() es un assertion
        expect(CrearCita).toHaveBeenCalled(); //Esperamos que crearCita, la funcion en juego haya sido llamada
        expect(CrearCita).toHaveBeenCalledTimes(1); //Se valida que la funcion sea llamada una vez

    }); 

