import Formulario from '../components/Formulario';
import React from 'react';
import { render, screen, cleanup, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import userEvent from '@testing-library/user-event';
import App from '../App';

//Se pueden hacer pruebas grandes, tal y como lo recomiendan los creadores de REACT, en el curso se ven pequenas por el alcance de los videos

//data-testid no se le puede agregar a los componentes, tiene que ser a una etiqueta en el dom

test('<App/> La aplicacion functiona', () => {
    render(<App />);

    expect(screen.getByText('Administrador de Pacientes')).toBeInTheDocument();
    expect(screen.getByTestId('nombre-app').textContent).toBe('Administrador de Pacientes');
    expect(screen.getByTestId('nombre-app').tagName).toBe('H1');

    expect(screen.getByText('Crear Cita')).toBeInTheDocument();
    expect(screen.getByText('No hay citas')).toBeInTheDocument();

    // const wrapper = render(<App />); //Para probar que se este montando el componente y ver sus elementos disponibles
    // wrapper.debug();
});

test('<App/> Agregar una cita y verificar el Heading', () => {
    render(<App />);

    userEvent.type(screen.getByTestId('mascota'), 'Hook'); //Sintaxis mas facil para tipear en los campos, mejor que fireEvent
    userEvent.type(screen.getByTestId('propierario'), 'Glenn'); //Sintaxis mas facil para tipear en los campos, mejor que fireEvent
    userEvent.type(screen.getByTestId('fecha'), '2021-09-10'); //Sintaxis mas facil para tipear en los campos, mejor que fireEvent
    userEvent.type(screen.getByTestId('hora'), '10:30'); //Sintaxis mas facil para tipear en los campos, mejor que fireEvent
    userEvent.type(screen.getByTestId('sintomas'), 'Solo duerme'); //Sintax

    const btnSubmit = screen.getByTestId('btn-submit');
    userEvent.click(btnSubmit); //Se valida el click

    // //*****------**** Otra cita
    // userEvent.type(screen.getByTestId('mascota'),'Hook2'); //Sintaxis mas facil para tipear en los campos, mejor que fireEvent
    // userEvent.type(screen.getByTestId('propierario'),'Fernando'); //Sintaxis mas facil para tipear en los campos, mejor que fireEvent
    // userEvent.type(screen.getByTestId('fecha'),'2021-09-10'); //Sintaxis mas facil para tipear en los campos, mejor que fireEvent
    // userEvent.type(screen.getByTestId('hora'),'10:30'); //Sintaxis mas facil para tipear en los campos, mejor que fireEvent
    // userEvent.type(screen.getByTestId('sintomas'),'Solo caga'); //Sintax

    // userEvent.click(btnSubmit); //Se valida el click



    //Que no se muestre la alerta
    const alerta = screen.queryByTestId('alerta'); //Con qieryByTestId se valida el codigo condicional, porque con getbyTestId estamos preguntando por una etiqueta que afuerza existe.
    expect(alerta).not.toBeInTheDocument(); //No se debe mostrar la alerta

    //Revisar por el titulo dinamico
    expect(screen.getByTestId('titulo-dinamico').textContent).toBe('Administra tus Citas');
    expect(screen.getByTestId('titulo-dinamico').textContent).not.toBe('No hay citas');
    //Si el snapshot falla cuando agrego otra cita, se debe eliminar el archivo snapshot actual, para generar otro actualizado
});


test('<App/> Verificar citas en el DOM', async () => {

    //Cuando vayamos a utilizar findAllbyTestid, debemos aplicar async await
    render(<App />);

    //El snapshot realiza una copia del elemento contenedor html para realizar las pruebas sobre este y no sobre el original
    const citas = await screen.findAllByTestId('cita');  //Se seleccionan multiples etiquetas, se reconoce que es un div

    // expect(citas).toMatchSnapshot(); //Se lee todo el html, snapshot permite crear un archivo nuevo con las respuestas y ahi hacer las pruebas

    expect(screen.getByTestId("btn-eliminar").tagName).toBe('BUTTON');
    expect(screen.getByTestId("btn-eliminar")).toBeInTheDocument();

    //Verificar alguna cita
    expect(screen.getByText('Hook')).toBeInTheDocument();

});


test('<App/> Eliminar la cita', () => {

    render(<App />);

    //simular el click

    const btnEliminar = screen.getByTestId('btn-eliminar');
    expect(btnEliminar.tagName).toBe('BUTTON');
    expect(btnEliminar).toBeInTheDocument();

    userEvent.click(btnEliminar);

    //Despues de eliminar, ya no deberia estar el boton, se elimino la cita
    expect(btnEliminar).not.toBeInTheDocument();
    //Verificar que la cita ya no este
    expect(screen.queryByText('Hook')).not.toBeInTheDocument();
    expect(screen.queryByTestId('cita')).not.toBeInTheDocument(); //Seria como todo el div de cada cita, se comprueba que no esta


});   
